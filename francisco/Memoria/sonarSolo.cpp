
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h> /*O_RDONLY*/
#include <unistd.h>  /*read*/
 


int initSensors() 
{

	//iniciar el archivo con:
	//sudo echo cape-bone-iio > /sys/devices/bone_capemgr.*/slots  
	//este archivo virtual contiene la ultima lectura del sensor 
	// en millivolts entre 0 y 1799 (hasta 1.8V)
    int fd = open("/sys/devices/ocp.3/helper.16/AIN1", O_RDONLY);
	return fd;
}

double readSensor(int fd)
{
	char buffer[8];
	int ret = read(fd, buffer, sizeof(buffer)); //lee el archivo
	
	if (ret == -1) {
	    printf("Error reading device\n");
	}
	else{
	    //lectura del archivo
	    buffer[ret] = 0;//lee
	    int mvLecturaADC = atoi(buffer);//almacena
	    lseek(fd, 0, 0);//reposiciona el puntero en el archivo

	    //1 metodo
	    double voltageadjust=1.8*2.78;
	    double involtage=(double)mvLecturaADC*voltageadjust/1000;
	    double distanciaCm=involtage*98*2.54+1;

	    //2 metodo
	    //double mvPorPulgada = (double)12/(double)6;
	    //double mvPorPulgada = 3.37/512;
		//double distanciaPulgadas = (double)(mvLecturaADC)/ (mvPorPulgada);
		
		//3 metodo
		//double distanciaCm = (mvLecturaADC * 512 * 2.54) / 1000;
		//double distanciaCm = (((mvLecturaADC)*1.8)/0.002148)*2.54;
//		double distanciaCm = (mvLecturaADC* 512 * 2.54)/1000;
	    return distanciaCm;
	    
	}
	return 0;
}


using namespace std;


int main () {
   
    int fd = initSensors();
    
    while(1) {
  
	double distance=readSensor(fd);
	usleep(500000);
	
	cout << distance << endl;
	
    }
}
