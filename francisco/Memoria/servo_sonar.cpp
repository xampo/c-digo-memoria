
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h> /*O_RDONLY*/
 
#include <sys/types.h>
#include <string.h>






/**
* distanciaSonar(int fd)
* fd es la descripcion del archivo que se obtiene al abrirlo utilizando
* fcntl.h 
* return un double con la distancia medida en centimetros
*/
double distanciaSonar(int fd)
{
	char buffer[8];
	int ret = read(fd, buffer, sizeof(buffer)); //lee el archivo
	
	if (ret == -1) {
	    printf("Error reading device\n");
	}
	else{
	    //lectura del archivo
	    buffer[ret] = 0;//lee
	    int mvLecturaADC = atoi(buffer);//almacena
	    lseek(fd, 0, 0);//reposiciona el puntero en el archivo

	    //1 metodo
	    //double voltageadjust=1.8*2.78;
	    //double involtage=(double)mvLecturaADC*voltageadjust/1000;
	    //double distanciaCm=involtage*98*2.54;

	    //2 metodo
	    //double mvPorPulgada = (double)12/(double)6;
	    //double mvPorPulgada = 3.37/512;
		//double distanciaPulgadas = (double)(mvLecturaADC)/ (mvPorPulgada);
		
		//3 metodo
		//double distanciaCm = (mvLecturaADC * 512 * 2.54) / 1000;
		//double distanciaCm = (((mvLecturaADC)*1.8)/0.002148)*2.54;
		double distanciaCm = (mvLecturaADC* 512 * 2.54)/1000;
		//std::cout << distanciaCm << std::endl;
	    return distanciaCm;
	    
	}
	return 0;
}

/**
* escribirDuty
* esta funcion escribe en un archivo un entero dado. puede ser utilizada para cualquier
* archivo que cumpla con las condiciones de las entradas.
*
* input:
*
* int frecuencia, lo que se escribira en el archivo, representa la frecuencia
* pwm para dar la señal al servo motor.
*
* int f_duty, la referencia al archivo previamente abierto con fcntl.h donde se escribira.
*/
void escribirInt(int frecuencia, int fd)
{

	//pasa un int a std:string
 	std::string str ;
 	char numstr[21]; // suficiente para numetos hasta 64-bits
	sprintf(numstr, "%d", frecuencia);
	str =  numstr;

	//escribe en el archivo
	char *dutyChar = new char[str.length() + 1];
	strcpy(dutyChar, str.c_str());
  	size_t length = strlen (dutyChar); 
  	write (fd, dutyChar, length); 
  	delete [] dutyChar;
  	lseek(fd, 0, 0);//reposiciona el puntero en el archivo
}

/**
* escribirRun
* con este metodo se escribira en el archivo run que recibe 1 o 0 para
* correr o detener las señales que recibe el servo motor.
*
* input:
*
* int correr, lo que se escribira en el archivo, representa la frecuencia
* pwm para dar la señal al servo motor.
*
* int f_duty, la referencia al archivo previamente abierto con fcntl.h donde se escribira.
*/
int escribirRun (int correr, int f_run)
{
	if (correr==1 || correr == 0)
	{
		escribirInt(correr,f_run);
	  	return 0;	
	}
	else
	{
		return -1;
	}
	
}

double promedioLecturas (int nLecturas, int f_AIN1)
{
	int i=0;
	double distance = 0;
	while(i<nLecturas)
	{
		distance = distance + distanciaSonar(f_AIN1);
		i++;
	}
	return (distance/nLecturas);

	
}
using namespace std;


int main () {


	//iniciar el archivo con el script shell en /home/francisco/initServoSonar.sh.
	//este archivo virtual contiene la ultima lectura del sensor 
	// en millivolts entre 0 y 1799 (hasta 1.8V)
    int f_AIN1 = open("/sys/devices/ocp.3/helper.16/AIN1", O_RDONLY);

   /* el script /home/francisco/initPWM.sh da valores a los archivos:
    * 0 > run
    * 20000000 > period
    * 2100000 > duty
    * run (1 o 0)
	*/
	int f_run = open("/sys/devices/ocp.3/pwm_test_P9_14.15/run", O_RDWR);
	int f_period = open("/sys/devices/ocp.3/pwm_test_P9_14.15/period", O_RDWR);
	int f_duty = open("/sys/devices/ocp.3/pwm_test_P9_14.15/duty", O_RDWR);

	int duty = 1400000;
   	escribirRun(1, f_run);
  	
  	// Constructs the new thread and runs it. Does not block execution.
    //thread t1(promedioLecturas, 20);

    //Makes the main thread wait for the new thread to finish execution, therefore blocks its own execution.
    
  	

    int i = 0;
    while(i<28) {
    	
	  	escribirInt(duty,f_duty);

	  	double distance = promedioLecturas(1000, f_AIN1);
		cout << "promedio " << (duty-1400000)/10000 <<" grados :"<<endl;
		cout << distance << endl;
		if (i<14)
		{
			duty=duty+100000;
		}
		else
		{
			duty=duty-100000;
		}
		i=i+1;
		//usleep(1000000);

		
		
		

		
	
    }
    close (f_duty); 
}
