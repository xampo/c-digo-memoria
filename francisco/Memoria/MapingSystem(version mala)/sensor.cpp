/* 
 * File:   sensor.cpp
 * Author: francisco
 * 
 * Created on 14 de octubre de 2015, 17:24
 */

#include "sensor.h"

#include <stdlib.h>
#include <unistd.h>

#include <fcntl.h> /*O_RDONLY*/
#include <sys/types.h>
#include <iostream>
#include <string.h>

#include <stdio.h>

#include <fstream>

#include <math.h>
#define PI 3.14159265
 
// constructor por defecto
sensor::sensor(void)
{
	f_AIN1 = open("/sys/devices/ocp.3/helper.16/AIN1", O_RDONLY);
	posActual = 0;
	f_run = open("/sys/devices/ocp.3/pwm_test_P9_14.15/run", O_RDWR);
	f_period = open("/sys/devices/ocp.3/pwm_test_P9_14.15/period", O_RDWR);
	f_duty = open("/sys/devices/ocp.3/pwm_test_P9_14.15/duty", O_RDWR);
	escribirRun(1, f_run);
	mover(0);
	maxRangoLectura=2;
}



void sensor::escribirInt(int frecuencia, int fd)
{

	//pasa un int a std:string
 	std::string str ;
 	char numstr[21]; // suficiente para numetos hasta 64-bits
	sprintf(numstr, "%d", frecuencia);
	str =  numstr;

	//escribe en el archivo
	char *dutyChar = new char[str.length() + 1];
	strcpy(dutyChar, str.c_str());
  	size_t length = strlen (dutyChar); 
  	write (fd, dutyChar, length); 
  	delete [] dutyChar;
  	lseek(fd, 0, 0);//reposiciona el puntero en el archivo
}


int sensor::escribirRun (int correr, int f_run)
{
	if (correr==1 || correr == 0)
	{
		escribirInt(correr,f_run);
	  	return 0;	
	}
	else
	{
		return -1;
	}
	
}

void sensor::setAIN1(int n_AIN1)
{
	f_AIN1 = n_AIN1;
}

double sensor::getDistanciaCm(void)
{
    char buffer[8];
	int ret = read(f_AIN1, buffer, sizeof(buffer)); //lee el archivo
	
	if (ret == -1) {
	    printf("Error reading device\n");
	}
	else{
	    //lectura del archivo
	    buffer[ret] = 0;//lee
	    int mvLecturaADC = atoi(buffer);//almacena
	    lseek(f_AIN1, 0, 0);//reposiciona el puntero en el archivo
		//double distanciaCm = (mvLecturaADC* 512 * 2.54)/1000;
		double voltageadjust=1.8*2.78;
	    double involtage=(double)mvLecturaADC*voltageadjust/1000;
	    double distanciaCm=involtage*98*2.54+1;
	    return distanciaCm;
	    
	}
	return 0;
}

int sensor::mover(int grados)
{
	if (grados%10 == 0)
	{
		int duty = 1400000 +((grados/10) * 100000);
		escribirInt(duty,f_duty);
		return 0;
	}
	else
	{
		return -1;
	}
	
}

void sensor::cerrarArchivos()
{
	close(f_AIN1);
	close(f_duty);
	close(f_period);
	close(f_run);
}
		


