/* 
 * File:   sensor.h
 * Author: francisco
 *
 * Created on 14 de octubre de 2015, 17:24
 */

#ifndef SENSOR_H
#define	SENSOR_H

/*
* Declaracion de la clase sensor la cual funcionara como una interfaz
* para obtener los datos desde el sensor ultrasonico y 
* obtener la lecturas
*/
class sensor
{
	private:
		
		int maxRangoLectura; //distancia maxima que puede leer
	 	int f_AIN1; //declaracion de archivo de entrada analoga
	 	int posActual;// posicion actual en grados sexagesimales [0-140]

	 	/*Archivos del servo motor*/
	 	int f_run;
		int f_period;
		int f_duty;

		
		/*Metodos provados*/
		/**
		* escribirInt
		* esta funcion escribe en un archivo un entero dado. puede ser utilizada para cualquier
		* archivo que cumpla con las condiciones de las entradas.
		* input:
		* int frecuencia, lo que se escribira en el archivo, representa la frecuencia
		* pwm para dar la señal al servo motor.
		* int f_duty, la referencia al archivo previamente abierto con fcntl.h donde se escribira.
		*/
	 	void escribirInt(int , int );

	 	/**
		* escribirRun
		* con este metodo se escribira en el archivo run que recibe 1 o 0 para
		* correr o detener las señales que recibe el servo motor.
		* input:
		* int correr, lo que se escribira en el archivo, representa la frecuencia
		* pwm para dar la señal al servo motor.
		* int f_duty, la referencia al archivo previamente abierto con fcntl.h donde se escribira.
		* output: -1 si hay un error de lectura, 0 si no los hay
		*/
	 	int escribirRun (int , int );
	public:
		// Constructor
		/**
		* sensor
		* constructor que inicializa la instancia de clase
		* abriendo el archivo para de las lecturas
		*/
		sensor(void);
		
		// metodos
		
		/**
		* setAIN1(int)
		* cambia la descripcion de archivo a leer
		* input: int  descripcion del archivo
		*/
		void setAIN1(int);
		
		/**
		* mover(int)
		* cambia la posicion del sensor en los angulos de
		* rotacion indicados, debe ser multiplo de 10
		* input: int  angulo de rotacion [0-140]
		* output: -1 cuando el angulo no es multiplo de 10
		* 0 si se ejecuta correctamente
		*/
		int mover(int);
		
		/**
		* getPosActual()
		* retorna la posicion actual en la que se encuentra 
		* el sensor, medido en grados º
		* output: int posActual 
		* 
		*/
		int getPosActual(void){return posActual;}
		
		
		/**
		* getMaxRangoLectura()
		* retorna el valor en centimetros del maximo que puede
		* leer el sensor.
		* output: int maxRangoLectura 
		* 
		*/
		int getMaxRangoLectura(void){return maxRangoLectura;}
		
		/**
		* getDistanciaCm()
		* funcion para obtener la distancia que esta leyendo
		* actualmente el sensor
		*/
		double getDistanciaCm(void);
		
		void cerrarArchivos(void);

		
};
#endif	/* SENSOR_H */
