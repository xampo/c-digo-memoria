/* 
 * File:   ElementoMatriz.cpp
 * Author: francisco
 * 
 * Created on 14 de octubre de 2015, 17:24
 */

#include "ElementoMatriz.h"
#include <iostream>	
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


ElementoMatriz::ElementoMatriz(void)
{
	 probabilidad = 0.5;
}

void ElementoMatriz::addAngulo(double a)
{
	listAngulos.push_back(a);
}

bool ElementoMatriz::tieneAngulo(double angulo)
{

	std::list<double>::iterator i = listAngulos.begin();
	while (i != listAngulos.end())
	{
            double a = *i;
            //std::cout<<"aOut: "<<a<<"anguloIn: "<<angulo<<std::endl;
	    if (a==angulo)
	    {
	       return true;
	    }
	    ++i;
	}
	return false;
}