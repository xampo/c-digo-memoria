
#ifndef __ALGORITMOPOSICION_H__
#define __ALGORITMOPOSICION_H__
#include "Mapa.h"

/*
* Declaracion de la clase que contiene el algoritmo de posicion de un objeto
* Usa las probabilidades para calcular la certeza de que existe un objeto 
* en cierta posición
*/
class AlgoritmoOcupacion
{
	private:

	 	

		/*Metodos provados*/
	
	public:
		// Constructor
		
		/**
		* AlgoritmoOcupacion
		* constructor que inicializa la instancia de clase
		* y sus atributos
		*/
		AlgoritmoOcupacion(void);

		/**
		* probabilidadRegion1
		* Este metodo sirve para calcular la probabilidad de ocupacion para los
		* elementos de la matriz de ocupacion que se encuentran en la Region 1,
		* la Region1 es los elementos estan a menos distancia de lo que se leyo.
		* 
		* input:
		* int R, rango maximo de lectura del sensor
		* int B, grados de abertura de transmicion del sensor
		* double lectura, lectura del sensor en centimetros
		* Matriz m, matriz de probabilidades
		* int i, posicion x en m del elemento en cuestion
		* int j, posicion y en m del elemento en cuestion
		*
		* ouput:
		* double con la probabilidad de ocupacion del elemento (i,j) en m
		*
		*/
		double probabilidadRegion1(int,int, double, Mapa, int, int);
		
		/**
		* probabilidadRegion2
		* Este metodo sirve para calcular la probabilidad de ocupacion para los
		* elementos de la matriz de ocupacion que se encuentran en la Region 2,
		* la Region2 son los elementos estan a la distancia de lo que se leyo +- 5 cm.
		* 
		* input:
		* int R, rango maximo de lectura del sensor
		* int B, grados de abertura de transmicion del sensor
		* double lectura, lectura del sensor en centimetros
		* Matriz m, matriz de probabilidades
		* int i, posicion x en m del elemento en cuestion
		* int j, posicion y en m del elemento en cuestion
		*/
		void probabilidadRegion2();
		void probabilidadRegion3();
		
};
#endif