/* 
 * File:   ElementoMatriz.h
 * Author: francisco
 *
 * Created on 14 de octubre de 2015, 17:24
 */

#ifndef ELEMENTOMATRIZ_H
#define	ELEMENTOMATRIZ_H

#include <list>




/*
* Declaracion de la clase sensor la cual funcionara como una interfaz
* para obtener los datos desde el sensor ultrasonico y 
* obtener la lecturas
*/
class ElementoMatriz
{
	private:
		std::list<double> listAngulos;//
		double probabilidad;//
	 	int posX; //
	 	int posY;// 
	 	double distanciaOrigen;//
	public:
	// Constructor
		/**
		* sensor
		* constructor que inicializa la instancia de clase
		* abriendo el archivo para de las lecturas
		*/
		ElementoMatriz(void);

		/**
		* GETTERS
		*/
		double getProbabilidad(void){return probabilidad;}
		std::list<double> getlistAngulos(void){return listAngulos;}
		int getPosX(void){return posX;}
		int getPosY(void){return posY;}
		double getDistanciaOrigen(void){return distanciaOrigen;}

		/**
		* SETTERS
		*/
		void setProbabilidad(double p){ probabilidad = p;}
		void setPosX(int x){ posX = x;}
		void setPosY(int y){ posY = y;}
		void setDistanciaOrigen(double d){ distanciaOrigen = d;}

		void addAngulo(double);

		/**
		* ve si en la lista de angulos esta el angulo pasado por el parametro
		*/
		bool tieneAngulo(double);
};
#endif	/* ELEMENTOMATRIZ_H */

