/* 
 * File:   main.cpp
 * Author: francisco
 *
 * Created on 14 de octubre de 2015, 17:07
 */

#include <cstdlib>

#include <fstream>
#include "Sensor.h"
#include <stdio.h>
#include <iostream>
#include <math.h>
#include "ElementoMatriz.h"
#include "Mapa.h"
#define PI 3.14159265
using namespace std;
#define R 510
#define MAX_OCUPPIED 0.98

/**
* probabilidadRegion1
* Este metodo sirve para calcular la probabilidad de ocupacion para los
* elementos de la matriz de ocupacion que se encuentran en la Region 1,
* la Region1 es los elementos estan a menos distancia de lo que se leyo.
* 
* input:
* int B, grados de abertura de transmicion del sensor
* double lectura, lectura del sensor en centimetros
* ElementoMatriz elto
*
* ouput:
* double con la probabilidad de ocupacion del elemento (i,j) en m
*
*/

double probabilidadRegion1(int b,  ElementoMatriz elto, double anguloMedicion)
{
	double a_i_j = 0;
	if (elto.getPosX()==0)
	{
		 a_i_j = fabs(anguloMedicion-90);
	}
	else if (elto.getPosX()<0)
	{
		 a_i_j = fabs((double)anguloMedicion-(180-(acos(fabs(elto.getPosX())/elto.getDistanciaOrigen())* 180 / PI)));
	}
	else
	{
		 a_i_j = fabs(anguloMedicion-(acos(elto.getPosX()/elto.getDistanciaOrigen())* 180 / PI));
	}
	double p_s_h = 1-((((R-elto.getDistanciaOrigen())/R) +((b - a_i_j)/b))/2);
	double p_h_s = p_s_h * elto.getProbabilidad()/((p_s_h * elto.getProbabilidad())+((1-p_s_h)*(1-elto.getProbabilidad())));
	return p_h_s;
        
	
 
}

/**
* probabilidadRegion2
* Este metodo sirve para calcular la probabilidad de ocupacion para los
* elementos de la matriz de ocupacion que se encuentran en la Region 2,
* la Region1 es los elementos estan a menos distancia de lo que se leyo.
* 
* input:
* int B, grados de abertura de transmicion del sensor
* double lectura, lectura del sensor en centimetros
* ElementoMatriz elto
*
* ouput:
* double con la probabilidad de ocupacion del elemento (i,j) en m
*
*/

double probabilidadRegion2(int b,  ElementoMatriz elto, double anguloMedicion)
{
	double a_i_j = 0;
	if (elto.getPosX()==0)
	{
		 a_i_j = fabs(anguloMedicion-90);
	}
	else if (elto.getPosX()<0)
	{
		 a_i_j = fabs(anguloMedicion-(180-(acos(fabs(elto.getPosX())/elto.getDistanciaOrigen())* 180 / PI)));
	}
	else
	{
		 a_i_j = fabs(anguloMedicion-(acos(elto.getPosX()/elto.getDistanciaOrigen())* 180 / PI));
	}
	
	double p_s_h = ((((R-elto.getDistanciaOrigen())/R) +((b - a_i_j)/b))/2)*MAX_OCUPPIED;
        
	double p_h_s = p_s_h * elto.getProbabilidad()/((p_s_h * elto.getProbabilidad())+((1-p_s_h)*(1-elto.getProbabilidad())));
	return p_h_s;
}

 
/**
* Clase principal que tiene la implementacion del algoritmo
*/

int main () {
	//inicializo el mapa(matriz)
	// se inicia el mapa con eltos con prob 0.5, cada elto con su distanciaOrigen, etc.
	Sensor s;
	Mapa mapa ;
	
    int k = 0;
    int duty = 0;// posicion inicial
    double distancia; //lectura actual
    std::cout << "Empieza a calcular probabilidades"<< std::endl;
    int lala=0;
    
    while(k<28) // 28(ida y vuelta del motor)
    {
        
    	// muevo el sensor a la primera posicion 
    	// 0 = 1ra pos = 20 grados
  		s.mover(duty);
		//usleep(100000);
		//obtengo la distancia
		
		distancia = s.getDistanciaCm();
		//std::cout<< "distancia: "<<distancia<<std::endl;
		//distancia=100;
		if(distancia>150)
		{
				distancia=150;
		}
		// Algoritmo:
		//  veo cual sera la region 1, 2 y 3
		//  la region 2 es distancia +- 5
		//  a los elementos del mapa que  tienen el aungulo(duty+20)
		//  y que esten a menos de distancia-5 del origen
		//  aplicar la formula de la region 1 
		//  si esta entre distancia - 5 y distancia + 5
		//  aplicar la formula de la region 2
		//  obviamente la formula de region 2 y region 1 detro de bayes
		//  para encontrar los elementos se debe hacer un for de la matriz mapa
		//  buscando que tenga el angulo 20
                
  		double b = mapa.getBFromDistancia((int)distancia);
       // std::cout << "b: " << b << std::endl;       
		for (int i = 0; i < 201; i++)
		{
			for (int j = 0; j < 401; j++)
			{
                            //std::cout<<mapa.getMatriz()[i][j].tieneAngulo(duty+20)<<std::endl;
				if(mapa.getMatriz()[i][j].tieneAngulo(duty+20))
				{
					if(mapa.getMatriz()[i][j].getDistanciaOrigen()< distancia-5)
					{
                                            
						//std::cout<<"REGION1"<<std::endl;
						mapa.getMatriz()[i][j].setProbabilidad(probabilidadRegion1(b, mapa.getMatriz()[i][j], duty+20));
					}
					else if (mapa.getMatriz()[i][j].getDistanciaOrigen()>= distancia-5 &&
						     mapa.getMatriz()[i][j].getDistanciaOrigen() <= distancia+5)
					{
						//std::cout<<"REGION2"<<std::endl;
						mapa.getMatriz()[i][j].setProbabilidad(probabilidadRegion2(b, mapa.getMatriz()[i][j], duty+20));
					}
				}
			}
			
		}

                
		k=k+1;
		if (k<14)
		{
			duty=duty+10;
		}
		else
		{
			duty=duty-10;
		}
	
    }
  

 
  std::ofstream myfile;
  myfile.open ("example.txt");
  
    for (int i = 0; i < 201; i++)
    {
        for (int j = 0; j < 401; j++)
        {
    
            myfile << mapa.getMatriz()[i][j].getProbabilidad() <<"\t" ;
                
        }
        myfile << "\n";
    }
    myfile.close();
     s.cerrarArchivos(); 
    return 0;


}
