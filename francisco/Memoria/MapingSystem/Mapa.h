/* 
 * File:   Mapa.h
 * Author: francisco
 *
 * Created on 14 de octubre de 2015, 17:23
 */

#ifndef MAPA_H
#define	MAPA_H

#include "ElementoMatriz.h"

#include <vector>
#include <list>
typedef struct {
        	double distancia;
        	double aperturaCm;
        	double b;
    	}datos;



/*
* Declaracion de la clase sensor la cual funcionara como una interfaz
* para obtener los datos desde el sensor ultrasonico y 
* obtener la lecturas
*/
class Mapa
{
	private:
         
   	std::list<datos> listDatos;
        
    	 ElementoMatriz** matriz;
    	 /**
    	 * Carga los datos desde el fichero "datos.txt" el cual contiene la apertura de la onda 
    	 * emitida por el sensor
    	 */
		void cargarDatos(void);
                
        /**

         * Esta funcion agrega a ElementoMatriz los angulos del sensor en los cuales
         * el elemento se encuentra dentro del cono de la onda emitida.
         * Pero esta funcion fue removida y implementada en el constructor del mapa.
         * @param 
         * @return 
         */
		ElementoMatriz agregarAngulosElto(ElementoMatriz);
		double sign (double,double, double,double, double, double);
		bool puntoEnTriangulo (double,double , double,double, double,double, double, double);
		double calcularDistanciaOrigen (int , int );
		
	public:
            
	// Constructor
		/**
		* sensor
		* constructor que inicializa la instancia de clase
		* abriendo el archivo para de las lecturas del sensor.
		*
		* agrega a ElementoMatriz los angulos del sensor en los cuales
        * el elemento se encuentra dentro del cono de la onda emitida, Estos datos se encuentran
        * en el archivo angulosElto.txt que fue generado por un script aparte.
		* 
		*/
		Mapa(void);
                
		ElementoMatriz **getMatriz(){return matriz;};

		double getBFromDistancia(int);
		/**
		* calcularRow calcula desde una posicion x del lugar
		* a una columna de la matriz
		* y [200,-200]
		*/
		int calcularRow(int );
		/**
		* calcularRow calcula desde una posicion y del lugar
		* a una columna de la matriz
		* x [-200,200]
		*/
		int calcularCol(int );
		/**
		* desde una columna de la matriz me devuelve la posicion x
		* del lugar
		* col [0,400]
		*/
		int calcularPosX(int);
		/**
		* desde una columna de la matriz me devuelve la posicion y
		* del lugar
		* row [0,400]
		*/
		int calcularPosY(int);
		
};

#endif	/* MAPA_H */

