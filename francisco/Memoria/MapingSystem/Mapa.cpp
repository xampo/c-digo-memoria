/* 
 * File:   Mapa.cpp
 * Author: francisco
 * 
 * Created on 14 de octubre de 2015, 17:23
 */

#include "Mapa.h"
#include "ElementoMatriz.h"
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>
#include <fstream> 
#include <list>
#define PI 3.14159265
#define ROWS 201
#define COLS 401
Mapa::Mapa(void)
{
  cargarDatos();
  std::cout <<"cargando mapa..."<< std::endl;
  
  std::string linea;
  std::ifstream infile;
  infile.open ("angulosElto.txt");
  datos aux;
  std::string delimiter = ";";



  matriz = new ElementoMatriz*[ROWS];
  for(int i = 0; i < ROWS; i++) 
  {
            matriz[i] = new ElementoMatriz[COLS]; 
            //std::cout <<"fila: "<<i<< std::endl;
            for(int j = 0; j < COLS; j++) 
            {
                    ElementoMatriz m;
                    m.setPosX(calcularPosX(j));
                    m.setPosY(calcularPosY(i));
                    m.setDistanciaOrigen(calcularDistanciaOrigen(calcularPosX(j),
                                                                 calcularPosY(i)));
                    //m = agregarAngulosElto(m);
                    if(getline(infile,linea)) // To get you all the lines.
                    {
                       // getline(infile,linea); // Saves the line in STRING.
                      size_t pos = 0;
                      std::string token;
                      if(linea!="")
                      {
                        while ((pos = linea.find(delimiter)) != std::string::npos) 
                        {
                            token = linea.substr(0, pos);

                            m.addAngulo(atof(token.c_str()));
                            linea.erase(0, pos + delimiter.length());
                        }
                      }
                      
                      //aux.aperturaCm = std::stof(linea);
                      //aux.b = atan(aux.aperturaCm/aux.distancia)* 180 / PI;
                      //listDatos.push_back(aux);
                    }





                    matriz[i][j]=m;
            }
  }
   infile.close(); 
  std::cout <<"Mapa cargado"<< std::endl;
}

ElementoMatriz Mapa::agregarAngulosElto(ElementoMatriz m)
{
  
      return m;
  
}
/*
ElementoMatriz Mapa::agregarAngulosElto(ElementoMatriz m)
{
    double izqX, derX, izqY, derY;
    int angulo;
    std::list<double> lista;
    
    
    angulo = 20;

      
    while(angulo<=160)
    {
        for (std::list<datos>::iterator ci = listDatos.begin(); ci != listDatos.end(); ++ci)
        { 


            datos dato = *ci;
                    izqX = dato.distancia *  cos ( angulo * PI / 180.0 ) - dato.aperturaCm * sin ( angulo * PI / 180.0);
                    izqY = dato.distancia *  sin ( angulo * PI / 180.0 ) + dato.aperturaCm * cos ( angulo * PI / 180.0);

                    derX = dato.distancia *  cos ( angulo * PI / 180.0 ) + dato.aperturaCm * sin ( angulo * PI / 180.0);
                    derY = dato.distancia *  sin ( angulo * PI / 180.0 ) - dato.aperturaCm * cos ( angulo * PI / 180.0);

                    if(puntoEnTriangulo(m.getPosX(),m.getPosY(),izqX,izqY,derX,derY,0,0))
                    {
                       
                            m.addAngulo(angulo);
                    }
                    

            }
  
        angulo = angulo + 10;
      
    }
      
    return m;
  } */  

double Mapa::sign (double p1_x,double p1_y, double p2_x,double p2_y, double p3_x, double p3_y)
{
    return (p1_x - p3_x) * (p2_y - p3_y) - (p2_x - p3_x) * (p1_y - p3_y);
}

bool Mapa::puntoEnTriangulo (double pt_x,double pt_y, double p1_x,double p1_y, double p2_x,double p2_y, double p3_x, double p3_y)
{
    bool b1, b2, b3;

    b1 = (sign( pt_x, pt_y, p1_x, p1_y, p2_x, p2_y) < 0.0);
    b2 =( sign( pt_x, pt_y,  p2_x, p2_y, p3_x, p3_y) < 0.0);
    b3 = (sign( pt_x, pt_y,  p3_x, p3_y, p1_x, p1_y) < 0.0);

    return ((b1 == b2) && (b2 == b3));
}

void Mapa::cargarDatos(void)
{

    std::string linea;
    std::ifstream infile;
    infile.open ("datos.txt");
  datos aux;
  std::string delimiter = ";";

    while(getline(infile,linea)) // To get you all the lines.
    {
       // getline(infile,linea); // Saves the line in STRING.
      size_t pos = 0;
      std::string token;
    
      while ((pos = linea.find(delimiter)) != std::string::npos) 
      {
          token = linea.substr(0, pos);
          aux.distancia = atof(token.c_str());
          linea.erase(0, pos + delimiter.length());
      }
      aux.aperturaCm = atof(linea.c_str());
      aux.b = atan(aux.aperturaCm/aux.distancia)* 180 / PI;
      listDatos.push_back(aux);
    }

  infile.close(); 
}
double Mapa::getBFromDistancia(int dist)
{
     
   std::list<datos>::iterator i = listDatos.begin();
  
  while (i != listDatos.end())
  {
    datos aux = *i;
    
      if  ((int)aux.distancia == dist)
      {
        return aux.b;
      }
      ++i;
  }
   
   return 0;
}

double Mapa::calcularDistanciaOrigen(int posX, int posY)
{
  
  return sqrt(pow(posX, 2) + pow(posY, 2));
}
/**
* calcularRow calcula desde una posicion x del lugar
* a una columna de la matriz
* y [200,-200]
*/
int Mapa::calcularRow(int y)
{
  return 200-y;
}

/**
* calcularRow calcula desde una posicion y del lugar
* a una columna de la matriz
* x [-200,200]
*/
int Mapa::calcularCol(int x)
{
  return x+200;
}
/**
* desde una columna de la matriz me devuelve la posicion x
* del lugar
* col [0,400]
*/
int Mapa::calcularPosX(int col)
{
  return col-200;
}
/**
* desde una columna de la matriz me devuelve la posicion y
* del lugar
* row [0,400]
*/
int Mapa::calcularPosY(int row)
{
  return 200-row;
}
