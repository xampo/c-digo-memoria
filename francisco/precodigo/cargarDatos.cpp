#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <vector>
#include <fstream> 
#include <list>
#define PI 3.14159265
typedef struct {
          double distancia;
          double aperturaCm;
          double b;
      }datos;

    

std::list<datos> listDatos;

double sign (double p1_x,double p1_y, double p2_x,double p2_y, double p3_x, double p3_y)
{
    return (p1_x - p3_x) * (p2_y - p3_y) - (p2_x - p3_x) * (p1_y - p3_y);
}

bool puntoEnTriangulo (double pt_x,double pt_y, double p1_x,double p1_y, double p2_x,double p2_y, double p3_x, double p3_y)
{
    bool b1, b2, b3;

    b1 = (sign( pt_x, pt_y, p1_x, p1_y, p2_x, p2_y) < 0.0);
    b2 =( sign( pt_x, pt_y,  p2_x, p2_y, p3_x, p3_y) < 0.0);
    b3 = (sign( pt_x, pt_y,  p3_x, p3_y, p1_x, p1_y) < 0.0);

    return ((b1 == b2) && (b2 == b3));
}
/**
* desde una columna de la matriz me devuelve la posicion x
* del lugar
* col [0,400]
*/
int calcularPosX(int col)
{
  return col-200;
}
/**
* desde una columna de la matriz me devuelve la posicion y
* del lugar
* row [0,400]
*/
int calcularPosY(int row)
{
  return 200-row;
}
void cargarDatos(void)
{
	std::string linea;
  std::ifstream infile;
	infile.open ("datos.txt");
	datos aux;
	std::string delimiter = ";";
 
    while(getline(infile,linea)) // To get you all the lines.
    {
       // getline(infile,linea); // Saves the line in STRING.
      size_t pos = 0;
		  std::string token;
		
  		while ((pos = linea.find(delimiter)) != std::string::npos) 
  		{
  		    token = linea.substr(0, pos);
  		    aux.distancia = std::stof(token);
  		    linea.erase(0, pos + delimiter.length());
  		}
  		aux.aperturaCm = std::stof(linea);
  		aux.b = atan(aux.aperturaCm/aux.distancia)* 180 / PI;
  		listDatos.push_back(aux);
    }

	infile.close();	

  std::list<datos>::iterator i = listDatos.begin();
  /*std::cout << "distancia \t aperturaCm \t b "<< std::endl;
  while (i != listDatos.end())
  {
    datos aux = *i;
      std::cout << aux.distancia << "\t" << aux.aperturaCm << "\t" << aux.b << std::endl;
      ++i;
  }*/


   std::ofstream myfile;
    myfile.open ("datos2.txt");
     double izqX, derX, izqY, derY;
    int angulo=20;
    while(angulo<=160)
    {
      for (std::list<datos>::iterator ci = listDatos.begin(); ci != listDatos.end(); ++ci)
      { 
        

        
        
              
          datos dato = *ci;
          izqX = dato.distancia *  cos ( angulo * PI / 180.0 ) - dato.aperturaCm * sin ( angulo * PI / 180.0);
          izqY = dato.distancia *  sin ( angulo * PI / 180.0 ) + dato.aperturaCm * cos ( angulo * PI / 180.0);
        
          derX = dato.distancia *  cos ( angulo * PI / 180.0 ) + dato.aperturaCm * sin ( angulo * PI / 180.0);
          derY = dato.distancia *  sin ( angulo * PI / 180.0 ) - dato.aperturaCm * cos ( angulo * PI / 180.0);
                  
          myfile<<angulo<<";"<<dato.distancia << ";" << dato.aperturaCm << ";" << dato.b << ";" << izqX<<";" << izqY<<";" << derX<<";" << derY<<std::endl;
          
          
      }
      angulo = angulo + 10;
  
  
      
    }
   myfile.close();


}

void agregarAngulosElto(std::list<datos> listDatos)
{
    double izqX, derX, izqY, derY;
    int angulo, posX, posY;
    std::list<double> lista;
    
    
    

    std::ofstream myfile;
    myfile.open ("angulosElto.txt");
    for (int i = 0; i < 201; ++i)
    {
      posY = calcularPosY(i);
       for (int j = 0; j < 401; ++j)
       {

          posX = calcularPosX(j);
          angulo = 20;
          while(angulo<=160)
          {
              for (std::list<datos>::iterator ci = listDatos.begin(); ci != listDatos.end(); ++ci)
              { 


                  datos dato = *ci;
                          izqX = dato.distancia *  cos ( angulo * PI / 180.0 ) - dato.aperturaCm * sin ( angulo * PI / 180.0);
                          izqY = dato.distancia *  sin ( angulo * PI / 180.0 ) + dato.aperturaCm * cos ( angulo * PI / 180.0);

                          derX = dato.distancia *  cos ( angulo * PI / 180.0 ) + dato.aperturaCm * sin ( angulo * PI / 180.0);
                          derY = dato.distancia *  sin ( angulo * PI / 180.0 ) - dato.aperturaCm * cos ( angulo * PI / 180.0);

                          if(puntoEnTriangulo(posX,posY,izqX,izqY,derX,derY,0,0))
                          {
                             myfile<<angulo<<";";
                                  
                          }
                          

                  }
                 
              angulo = angulo + 10;
            
          }
          myfile<<std::endl; 


       }
    }
    
     myfile.close();
  
}


int main () {

  cargarDatos();
  agregarAngulosElto(listDatos);

  return 0;
  }