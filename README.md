# LÉAME #

###¿Para qué es este repositorio?###
Este repositorio está dedicado a respaldar el trabajo que hice para mi proyecto de título el cual consiste en  la generación de un mapa 2d utilizando un sensor ultrasónico y un servo motor, alojando el programa en una BeagleBoneBlack.

###Estructura###

- francisco
    + initPWM.sh 
    + initServoSonar.sh
    + Memoria
        - MapingSystem  - carpeta -
        - servo_sonar.cpp
        - sonar_solo.cpp
    + precodigo
    
###¿Cómo funciona?###

Primero, todos los archivos deben estar cargados en la beaglebone. El servo motor y el sensor ultrasónico deben estar 
instalados como corresponda.
Ejecutar los iniciadores .sh y luego se procede a ejecutar el archivo main que se encuentra en la carpeta MapingSystem. Si se tienen problemas con los archivos de inicio, verificar el puerto de la beaglebone black y corregirlos. Para más detalles revisar el informe del proyecto que se encuentra en las dependencias de la Universidad de Talca de Chile.

# README #

###¿What is this repository for?###
This repository is dedicated to backup my work, I did for my grade project. Which consist in generating a 2D map, using a ultrasonic sensor and a servomotor. Hosting the program on a BeagleBoneBlack

###Estructure###

- francisco
    + initPWM.sh 
    + initServoSonar.sh
    + Memoria
        - MapingSystem  - carpeta -
        - servo_sonar.cpp
        - sonar_solo.cpp
    + precodigo
    
###How it works?###

First of all, every file should be loaded on the BeagleBone. The servomotor and ultrasonic sensor should be installed correspondingly.
Run the initiators .sh, and then proceeds to run the main file located in the folder "MapingSystem".
If you have problems with init files, verify the BeagleBone's ports and correct them.
For more details, check the project report located on the Universidad de Talca dependencies in Chile.